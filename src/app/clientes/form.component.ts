import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente()
  public titulo:string ="Crear Cliente"

  constructor(
    private clienteService:ClienteService,
    private router:Router,
    private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente()
  }
  //CARGAR CLIENTE O EDITAR 2
  cargarCliente():void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.clienteService.getCliente(id).subscribe((cliente) => this.cliente = cliente)
      }
    })
  }

  //CREAR CLI CLIENTE
  public create(): void {
    console.log("Clicked!")
    console.log(this.cliente)
    this.clienteService.create(this.cliente)
    .subscribe(cliente =>{
      this.router.navigate(['/clientes'])
      swal.fire('Nuevo Cliente',`Cliente ${cliente.nombre} creado con éxito!`,'success' )
    }
    )
  }
//METODO UPDATE 2021
update():void {
  this.clienteService.update(this.cliente)
  .subscribe(cliente =>{
    this.router.navigate(['/clientes'])
    swal.fire('Cliente Actualizado',`Cliente ${cliente.nombre} actualizado con éxito!`,'success' )
  })
}

}
